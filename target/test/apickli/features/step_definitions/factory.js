var apickli = require('apickli');
var config = require('../../config/config.json');

var defaultBasePath = config.merchant.basepath;
var defaultDomain = config.merchant.domain;

console.log('merchant api: [' + config.merchant.domain + ', ' + config.merchant.basepath + ']');

var getNewApickliInstance = function(basepath, domain) {
	basepath = basepath || defaultBasePath;
	domain = domain || defaultDomain;

	return new apickli.Apickli('https', domain + basepath);
};

exports.getNewApickliInstance = getNewApickliInstance;
