/* jshint node:true */
'use strict';

var factory = require('./factory.js');
var config = require('../../config/config.json');

var invalidClientId = config.merchant.invalidClientId;
var clientId = config.merchant.clientId;
var clientSecret = config.merchant.clientSecret;

module.exports = function () {
    // cleanup before every scenario
    this.Before(function (scenario, callback) {
        this.apickli = factory.getNewApickliInstance();
        this.apickli.storeValueInScenarioScope("invalidClientId", invalidClientId);
        this.apickli.storeValueInScenarioScope("clientId", clientId);
        this.apickli.storeValueInScenarioScope("clientSecret", clientSecret);
        callback();
    });
};

