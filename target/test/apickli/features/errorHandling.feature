@intg
@error-handling
Feature: Error handling
	As an API consumer
	I want consistent and meaningful error responses
	So that I can handle the errors correctly

	@invalid-hmac
    Scenario: GET with invalid hmac
        Given I set clientId header to `clientId`
        And I set nonce header to `nonce`
        And I set timestamp header to `timestamp`
        And I set Authorization header to invalid-hmac
        When I GET /ping
        Then response code should be 401
        And response header Content-Type should be application/json
        And response body path $.code should be 100003
        And response body path $.message should be Invalid HMAC

	@invalid-client-id
    Scenario: GET with invalid clientId
        Given I set clientId header to badapikey
        When I GET /ping
        Then response code should be 401
        And response header Content-Type should be application/json
        And response body path $.code should be 100005
        And response body path $.message should be Missing or invalid Application Identifier

	@missing-client-id
    Scenario: GET with missing clientId
        When I GET /ping
        Then response code should be 401
        And response header Content-Type should be application/json
        And response body path $.code should be 100005
        And response body path $.message should be Missing or invalid Application Identifier
    
    @invalid-clientid-for-resource
    Scenario: GET with invalid clientId for resource
        Given I set clientId header to `invalidClientId`
        When I GET /ping
        Then response code should be 401
        And response header Content-Type should be application/json
        And response body path $.code should be 100005
        And response body path $.message should be Missing or invalid Application Identifier

	
	@missing-nonce
    Scenario: GET with missing required header nonce
        Given I set clientId header to `clientId`
        And I set timestamp header to `timestamp`
        And I set Authorization header to invalid-hmac
        When I GET /ping
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 100001
        And response body path $.message should be One or more required headers are missing

	@missing-timestamp
    Scenario: GET with missing required header timestamp
        Given I set clientId header to `clientId`
        And I set nonce header to `nonce`
        And I set Authorization header to invalid-hmac
        When I GET /ping
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 100001
        And response body path $.message should be One or more required headers are missing

	@missing-authorization
    Scenario: GET with missing required header Authorization
        Given I set clientId header to `clientId`
        And I set nonce header to `nonce`
        And I set timestamp header to `timestamp`
        When I GET /ping
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 100001
        And response body path $.message should be One or more required headers are missing

	@foo
    Scenario: GET /foo request
        Given I set clientId header to `clientId`
        When I use HMAC and GET /foo
        Then response code should be 404
        And response header Content-Type should be application/json
        And response body path $.code should be 100006
        And response body path $.message should be Resource not found
        
	@foobar
    Scenario: GET /foo/bar request
        Given I set clientId header to `clientId`
        When I use HMAC and GET /foo/bar
        Then response code should be 404
        And response header Content-Type should be application/json
        And response body path $.code should be 100006
        And response body path $.message should be Resource not found
        
    @invalid-json
    Scenario: POST charge with invalid content
        Given I set clientId header to `clientId`
        And I set content-type header to application/json
        And I set body to {'hello':'world}
        When I use HMAC and POST to /ping
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 100007
        And response body path $.message should be Invalid message request format

	@wrong-content-type
    Scenario: POST charge with wrong content-type
        Given I set clientId header to `clientId`
        And I set content-type header to text/xml
        And I set body to {"hello":"world"}
        When I POST to /charges
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 100007
        And response body path $.message should be Invalid message request format
        And response body path $.detail should be Invalid JSON request in message request, check syntax
    
    @missing-content-type
    Scenario: POST charge with missing content-type
        Given I set clientId header to `clientId`
        And I set body to {"hello":"world"}
        When I POST to /charges
        Then response code should be 400
        And response header Content-Type should be application/json
        And response body path $.code should be 100001
        And response body path $.message should be One or more required headers are missing
