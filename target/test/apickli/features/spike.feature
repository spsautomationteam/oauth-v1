@spike
Feature: Spike Arrest
	As an API manager
	I want to reject concurrent requests after a certain rate
	So I can protect backend

    @spike
	Scenario: Testing spike arrest response
		Given I have a valid credentials
		When I use HMAC and get /ping 50 times
		Then I should get at least 1 429 error with message "Rate limit exceeded" and code "100004"

