@intg
Feature: OAuth API proxy 
	As Developer
    I want to Post a request for token
    So I can get access_token
    
	@post-token
    Scenario: POST token with valid content
        Given I set clientId header to `clientId`
        And I set content-type header to application/json
        And I set body to { "grant_type": "user_identity", "client_id": "`clientId`", "identity": "407416"}
        When I use HMAC and POST to /tokens
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body path $.access_token should be [^\s\.,!?]+
        And response body path $.expires_in should be 3600
        And response body path $.refresh_token should be [^\s\.,!?]+
        And response body path $.token_type should be Bearer
        And response body path $.scope should be core-submit-applications

	@post-token-invalid-clientId
    Scenario: POST token with invalid clientId
        Given I set clientId header to `REPLACE-INVALID-CLIENTID`
        And I set content-type header to application/json
        And I set body to { "grant_type": "user_identity", "client_id": "`clientId`", "identity": "407416"}
        When I use HMAC and POST to /tokens
        Then response code should be 401
        And response body path $.detail should be A valid Application ID is required in header parameter clientId.
		
	@post-token-invalid-content-type
    Scenario: POST token with invalid content type
        Given I set clientId header to `clientId`
        And I set content-type header to invalid-content-type
        And I set body to { "grant_type": "user_identity", "client_id": "`clientId`", "identity": "407416"}
        When I use HMAC and POST to /tokens
        Then response code should be 400
        And response body path $.detail should be Required headers: 'Authorization', 'nonce', 'timestamp' and 'Content-Type' for POST and PUT
		
	@post-token-invalid-clientId-in-body
    Scenario: POST token with invalid clientId-in-body
        Given I set clientId header to `clientId`
        And I set content-type header to application/json
        And I set body to { "grant_type": "user_identity", "client_id": "`REPLACE-INVALID-CLIENTID`", "identity": "407416"}
        When I use HMAC and POST to /tokens
        Then response code should be 400
        And response body path $.detail should be Required content: grant_type, client_id.
		
	@post-token-no-clientId-in-body
    Scenario: POST token with no client Id in body
        Given I set clientId header to `clientId`
        And I set content-type header to application/json
        And I set body to { "grant_type": "user_identity", "identity": "407416"}
        When I use HMAC and POST to /tokens
        Then response code should be 400
        And response body path $.detail should be Required content: grant_type, client_id.
		
	@post-token-invalid-grantType-in-body
    Scenario: POST token with invalid grant Type
        Given I set clientId header to `clientId`
        And I set content-type header to application/json
        And I set body to { "grant_type": "user", "client_id": "`clientId`", "identity": "407416"}
        When I use HMAC and POST to /tokens
        Then response code should be 400
        And response body path $.detail should be invalid_grant. Invalid grant_type specified
		
	@post-token-no-grantType-in-body
    Scenario: POST token with no grant Type
        Given I set clientId header to `clientId`
        And I set content-type header to application/json
        And I set body to { "client_id": "`clientId`", "identity": "407416"}
        When I use HMAC and POST to /tokens
        Then response code should be 400
        And response body path $.detail should be Required content: grant_type, client_id.
		
	@post-token-invalid-grantType-refreshToken_in-body
    Scenario: POST token with invalid grant Type (refresh_token)
        Given I set clientId header to `clientId`
        And I set content-type header to application/json
        And I set body to { "grant_type": "refresh_token", "client_id": "`clientId`", "identity": "407416"}
        When I use HMAC and POST to /tokens
        Then response code should be 400
        And response body path $.detail should be invalid_grant. The data provided for grant type refresh_token is missing, invalid, or expired
		
	@post-token-invalid-grantType-authorizationCode_in-body
    Scenario: POST token with invalid grant Type (authorization code)
        Given I set clientId header to `clientId`
        And I set content-type header to application/json
        And I set body to { "grant_type": "authorization_code", "client_id": "`clientId`", "identity": "407416"}
        When I use HMAC and POST to /tokens
        Then response code should be 400
        And response body path $.detail should be invalid_request. Missing parameter: redirect_uri
		
	@post-token-invalid-identity-in-body
    Scenario: POST token with invalid identity
        Given I set clientId header to `clientId`
        And I set content-type header to application/json
        And I set body to { "grant_type": "user_identity", "client_id": "`clientId`", "identity": "1234"}
        When I use HMAC and POST to /tokens
        Then response code should be 400
        And response body path $.detail should be invalid_request. The user identity specified is invalid
		
	@post-token-no-identity-in-body
    Scenario: POST token with no identity
        Given I set clientId header to `clientId`
        And I set content-type header to application/json
        And I set body to { "grant_type": "user_identity", "client_id": "`clientId`"}
        When I use HMAC and POST to /tokens
        Then response code should be 400
        And response body path $.detail should be invalid_request. Missing parameter: identity
       
	@Post-token-invalid-hmac
       Scenario: POST with invalid hmac

        Given I set clientId header to `clientId`
        And I set content-type header to application/json
        And I set nonce header to `nonce`
        And I set timestamp header to `timestamp`
        And I set body to { "grant_type": "user_identity", "client_id": "`clientId`", "identity": "407416"}
        And I set Authorization header to invalid-hmac
        When I POST to /tokens
        Then response code should be 401
        And response header Content-Type should be application/json
        And response body path $.code should be 100003
        And response body path $.message should be Invalid HMAC
		
	@post-token-missing-Authorizaton
    Scenario: POST with missing Authorization
        
       Given I set clientId header to `clientId`
       And I set content-type header to application/json
       And I set nonce header to 'nonce'
       And I set timestamp header to 'timestamp'      
       And I set body to { "grant_type": "user_identity", "client_id": "`clientId`", "identity": "407416"} 
       When I POST to /tokens
       Then response code should be 400
       And response header Content-Type should be application/json
       And response body path $.code should be 100001
       And response body path $.message should be One or more required headers are missing       
       

     @post-token-missing-content-type
       Scenario: POST with missing content type
       
       Given I set clientId header to `clientId`
       And I set nonce header to 'nonce'
       And I set timestamp header to 'timestamp'
       And I set authorization header to 'Authorization'      
       And I set body to { "grant_type": "user_identity", "client_id": "`clientId`", "identity": "407416"} 
       When I use HMAC and POST to /tokens
       Then response code should be 400       
       And response body path $.code should be 100001
       And response body path $.message should be One or more required headers are missing