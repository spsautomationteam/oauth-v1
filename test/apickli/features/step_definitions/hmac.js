/* jslint node: true */
'use strict';

// Create a new singleton
var hmacTools = new (require("../../../hmacTools.js"))();
var async = require('async');

module.exports = function () { 

	var replaceVariables = function(apickli, s){
        if (s.indexOf('`') > -1) {
            // apickli doesn't replace the variable with the actual value until later in the process
            // so, this is necessary for hmac calculation
            var variableName = s.match(/`(.*?)`/);
            return s.replace(variableName[0], apickli.scenarioVariables[variableName[1]]);
            // variableName[0] includes the ` chars, [1] doesn't
        }
        return s;
    }
    
    var setupCommon = function(apickli, verb, pathSuffix) {
		var timestamp = Date.now() / 1000 + '';
		var nonce = hmacTools.nonce(12);
		var url = apickli.domain + replaceVariables(apickli, pathSuffix);
		var clientSecret = apickli.scenarioVariables['clientSecret'];
		var body = apickli.requestBody;
		var hmac = hmacTools.hmac( clientSecret, verb, url, body, '', nonce, timestamp);
        //apickli.addRequestHeader('nonce', nonce);
        apickli.headers['nonce'] = nonce;
        // use this array notation instead of addRequestHeader
        // the former REPLACES any existing headers (y)
        // the latter APPENDS to existing headers (n)
        apickli.headers['timestamp'] = timestamp;
        apickli.headers['authorization'] = hmac;
       // apickli.headers['clientId'] = apickli.scenarioVariables.clientId;
	};

	this.When(/^I use HMAC and GET (.*)$/, function (pathSuffix, callback) {
		setupCommon(this.apickli, "GET", pathSuffix);
		this.apickli.get(pathSuffix, function(err, response) {
			callback(err);
		});
    });

	this.When(/^I use HMAC and PUT to (.*)$/, function (pathSuffix, callback) {
        setupCommon(this.apickli, "PUT", pathSuffix);
        this.apickli.put(pathSuffix, function(err, response) {
            callback(err);
        });
    });

	this.When(/^I use HMAC and POST to (.*)$/, function (pathSuffix, callback) {
        setupCommon(this.apickli, "POST", pathSuffix);
        this.apickli.post(pathSuffix, function(err, response) {
            console.log(response);
			callback(err);
        });
    });

	this.When(/^I use HMAC and DELETE (.*)$/, function (pathSuffix, callback) {
        setupCommon(this.apickli, "DELETE", pathSuffix);
        this.apickli.delete(pathSuffix, function(err, response) {
            callback(err);
        });
    });

	this.When(/^I use HMAC and get (.*) (\d+) times$/, function(pathSuffix, count, callback) {

		setupCommon(this.apickli, "GET", pathSuffix );

		var self = this;

		async.times(count, function(n, next) {
			self.apickli.get(pathSuffix, function(err, response) {
				next(null, response);
			});
		}, function(err, responses) {
			self.apickli.scenarioVariables.allResponses = responses;
			callback();
		});
	});

    this.Given(/^I have a valid credentials$/, function (callback) {
         // Write code here that turns the phrase above into concrete actions
         callback();
       });
};

