@intg
Feature: API proxy target error handling
    As an API developer, 
    I want to test error responses when the target is unavailable,
    So I know they are consistent.

Response:
    {
      "code": "100000",
      "message": "Service is currently not available",
      "info": "https://developer.sage.com/docs/bankcard/v1/errors/100000",
      "detail": "Service is temporarily unavailable."
    }

#This code will be un commented when status is implemented.
# @intg
#   Scenario: Target error response
#       Given I set clientId header to `clientId`
#       And I set X-mock header to true
#       When I use HMAC and GET /status
#       Then response code should be 503
#       And response body path $.code should be 100000
#       And response body path $.message should be Service is currently not available
#
